package com.guenegom.swedysh.core.helpers;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.Resource;
import org.springframework.test.context.junit4.SpringRunner;

import java.io.IOException;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(SpringRunner.class)
public class FileHelperTest {

    @Value("classpath:/com/guenegom/swedysh/core/helpers/fileHelperTest.txt")
    private Resource testResource;

    @Test
    public void testReadStringFromFile() throws IOException {
        String expected = "This is an expected file content";

        assertThat(testResource).isNotNull();
        String actual = FileHelper.readStringFromFile(testResource);

        assertThat(actual).isNotBlank().isEqualTo(expected);
    }
}
