package com.guenegom.swedysh.core.helpers;

import com.guenegom.swedysh.core.api.test.MyTestStatus;
import com.guenegom.swedysh.core.model.*;
import com.guenegom.swedysh.core.model.enums.FailedActionStrategy;
import com.guenegom.swedysh.core.model.enums.StatusEqualityStrategy;
import com.guenegom.swedysh.core.utils.SwedyshUtils;
import com.guenegom.swedysh.core.workflow.SwedyshConstants;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.Resource;
import org.springframework.test.context.junit4.SpringRunner;

import java.io.IOException;
import java.util.List;
import java.util.stream.Collectors;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(SpringRunner.class)
public class DeserializationHelperTest {

    @Value("classpath:/com/guenegom/swedysh/core/helpers/deserializationHelperTest_1.yml")
    private Resource yamlResource1;

    @Test
    public void testGetYamlAsPojo_1() throws IOException {
        String yaml = FileHelper.readStringFromFile(yamlResource1);
        SwedyshWorkflow actual = DeserializationHelper.getYamlStringAsPojo(yaml, SwedyshWorkflow.class);

        assertThat(actual).isNotNull();
        assertThat(actual.getGlobals()).isNotNull();
        assertThat(actual.getGlobals().getFactory()).isEqualTo("myTestStatusFactory");
        assertThat(actual.getGlobals().getOnSourceEqualsTarget()).isEqualTo(StatusEqualityStrategy.IGNORE);
        assertThat(actual.getGlobals().getDefaults()).isNotNull();
        assertThat(actual.getGlobals().getDefaults().getOnFailedAction()).isEqualTo(FailedActionStrategy.FAIL);
        assertThat(actual.getGlobals().getDefaults().getDisplay()).isTrue();
        assertThat(actual.getGlobals().getDefaults().getCanForce()).isFalse();

        assertThat(actual.getTransitions()).isNotNull().isNotEmpty().hasSize(4);
        assertThat(actual.getTransitions().stream().map(SwedyshTransitionConfiguration::getSource).collect(Collectors.toList()))
                .containsExactly("DRAFT", "TO_PREPARE", "ASSIGNED_FOR_PREPARATION", SwedyshConstants.ANY_JOKER);
        assertThat(actual.getTransitions().stream().map(SwedyshTransitionConfiguration::getTarget).collect(Collectors.toList()))
                .containsExactly("TO_PREPARE", "ASSIGNED_FOR_PREPARATION", "LOCKED_FOR_PREPARATION", "CANCELLED");
        assertThat(actual.getTransitions().stream().map(Level2DefaultConfiguration::getOnFailedAction).collect(Collectors.toList()))
                .containsExactly(null, null, null, FailedActionStrategy.IGNORE);
        assertThat(actual.getTransitions().stream().map(Level2DefaultConfiguration::getDisplay).collect(Collectors.toList()))
                .containsExactly(null, Boolean.FALSE, Boolean.FALSE, null);

        List<SwedyshTransitionConfiguration> actualMatches = SwedyshUtils.findMatchingTransitions(actual.getTransitions(), MyTestStatus.TO_PREPARE, MyTestStatus.ASSIGNED_FOR_PREPARATION);
        assertThat(actualMatches).hasSize(1);
        SwedyshTransitionConfiguration actualTransition = actualMatches.get(0);
        assertThat(actualTransition.getChecks().stream().map(SwedyshCheckConfiguration::getId)).containsExactly("myTestMandatoryDataCheck", "myTestDatesConsistencyCheck");
        assertThat(actualTransition.getChecks().stream().map(SwedyshCheckConfiguration::getCanForce)).containsExactly(null, Boolean.TRUE);
        assertThat(actualTransition.getActions().stream().map(SwedyshActionConfiguration::getId)).containsExactly("myTestAuditAction");

        List<SwedyshTransitionConfiguration> actualMatches2 = SwedyshUtils.findMatchingTransitions(actual.getTransitions(), MyTestStatus.ASSIGNED_FOR_PREPARATION, MyTestStatus.LOCKED_FOR_PREPARATION);
        assertThat(actualMatches2).hasSize(1);
        SwedyshTransitionConfiguration actualTransition2 = actualMatches2.get(0);
        assertThat(actualTransition2.getActions().stream().map(SwedyshActionConfiguration::getId)).containsExactly("myTestAudit2Action", "myTestNotificationAction");

        List<SwedyshTransitionConfiguration> actualMatches3 = SwedyshUtils.findMatchingTransitions(actual.getTransitions(), MyTestStatus.DRAFT, MyTestStatus.TO_PREPARE);
        assertThat(actualMatches).hasSize(1);
        SwedyshTransitionConfiguration actualTransition3 = actualMatches3.get(0);
        assertThat(actualTransition3.getCanForce()).isTrue();

    }
}