package com.guenegom.swedysh.core.api.test;

import com.guenegom.swedysh.core.api.SwedyshStatus;

public enum MyTestStatus implements SwedyshStatus {
    DRAFT,
    TO_PREPARE,
    ASSIGNED_FOR_PREPARATION,
    LOCKED_FOR_PREPARATION,
    READY,
    TO_EXECUTE,
    ASSIGNED_FOR_EXECUTION,
    LOCKED_FOR_EXECUTION,
    EXECUTED,
    CANCELLED;

    @Override
    public String getCode() {
        return name();
    }
}
