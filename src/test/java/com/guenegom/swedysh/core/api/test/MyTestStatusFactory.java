package com.guenegom.swedysh.core.api.test;

import com.guenegom.swedysh.core.api.SwedyshStatusFactory;

public class MyTestStatusFactory implements SwedyshStatusFactory<MyTestStatus> {
    @Override
    public MyTestStatus instantiate(String code) {
        return MyTestStatus.valueOf(code);
    }
}
