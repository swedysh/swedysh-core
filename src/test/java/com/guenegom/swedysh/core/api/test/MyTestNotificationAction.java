package com.guenegom.swedysh.core.api.test;

import com.guenegom.swedysh.core.api.SwedyshAction;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

@Component
@Slf4j
public class MyTestNotificationAction implements SwedyshAction<MyTestPojo> {
    @Override
    public MyTestPojo execute(MyTestPojo toModify) {
        LOG.info("I don't do a thing. I notify some test neighbour asynchronously...");
        return toModify;
    }
}
