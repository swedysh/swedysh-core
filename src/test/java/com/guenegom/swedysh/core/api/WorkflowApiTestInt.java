package com.guenegom.swedysh.core.api;

import com.guenegom.swedysh.core.api.test.MyTestPojo;
import com.guenegom.swedysh.core.api.test.MyTestStatus;
import com.guenegom.swedysh.core.exceptions.ConfirmationRequiredException;
import com.guenegom.swedysh.core.exceptions.FailedCheckException;
import com.guenegom.swedysh.core.exceptions.InvalidTransitionException;
import com.guenegom.swedysh.test.utils.TestUtils;
import org.assertj.core.api.Assertions;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.failBecauseExceptionWasNotThrown;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = MyTestConfiguration.class)
public class WorkflowApiTestInt {

    @Autowired
    private WorkflowApi<MyTestPojo> api;

    @Test
    public void testExecuteTransition1_SimpleAuthorizedTransition() {
        MyTestPojo before = new MyTestPojo().setId(Long.valueOf("1001")).setName("pojo 1").setStatus(MyTestStatus.DRAFT);
        MyTestPojo after = new MyTestPojo().setId(Long.valueOf("1002")).setName("pojo 2").setStatus(MyTestStatus.TO_PREPARE);

        MyTestPojo actual = api.executeTransition(before, after);

        assertThat(actual).isNotNull();
    }

    @Test(expected = InvalidTransitionException.class)
    public void testExecuteTransition2_UnauthorizedTransition() {
        MyTestPojo before = new MyTestPojo().setId(Long.valueOf("1001")).setName("pojo 1").setStatus(MyTestStatus.DRAFT);
        MyTestPojo after = new MyTestPojo().setId(Long.valueOf("1002")).setName("pojo 2").setStatus(MyTestStatus.EXECUTED);

        api.executeTransition(before, after);

        failBecauseExceptionWasNotThrown(InvalidTransitionException.class);
    }

    @Test
    public void testExecuteTransition3_CheckedAuthorizedTransition() {
        MyTestPojo before = new MyTestPojo().setId(Long.valueOf("1001")).setName("pojo 1").setStatus(MyTestStatus.TO_PREPARE);
        MyTestPojo after = new MyTestPojo().setId(Long.valueOf("1002")).setName("pojo 2").setStatus(MyTestStatus.ASSIGNED_FOR_PREPARATION).setMandatoryCount(5);

        MyTestPojo actual = api.executeTransition(before, after);

        assertThat(actual).isNotNull();
    }

    @Test(expected = InvalidTransitionException.class)
    public void testExecuteTransition4_CheckedUnauthorizedTransition() {
        MyTestPojo before = new MyTestPojo().setId(Long.valueOf("1001")).setName("pojo 1").setStatus(MyTestStatus.TO_PREPARE);
        MyTestPojo after = new MyTestPojo().setId(Long.valueOf("1002")).setName("pojo 2").setStatus(MyTestStatus.ASSIGNED_FOR_PREPARATION);

        MyTestPojo actual = api.executeTransition(before, after);

        failBecauseExceptionWasNotThrown(FailedCheckException.class);
    }

    @Test
    public void testGestCandidates1() {
        MyTestPojo current = new MyTestPojo().setId(Long.valueOf("1001")).setName("pojo 1").setStatus(MyTestStatus.TO_PREPARE);

        List<SwedyshStatus> actual = api.getCandidates(current);

        Assertions.assertThat(actual).isNotEmpty().hasSize(2);
        Assertions.assertThat(actual).containsExactly(MyTestStatus.ASSIGNED_FOR_PREPARATION, MyTestStatus.READY);
    }

    @Test
    public void testGestCandidates2() {
        MyTestPojo current = new MyTestPojo().setId(Long.valueOf("1001")).setName("pojo 1").setStatus(MyTestStatus.ASSIGNED_FOR_PREPARATION);

        List<SwedyshStatus> actual = api.getCandidates(current);

        Assertions.assertThat(actual).isEmpty();
    }

    @Test(expected = ConfirmationRequiredException.class)
    public void testExecuteTransition5_ConfirmationRequired() {
        MyTestPojo before = new MyTestPojo().setId(Long.valueOf("1001")).setName("pojo 1").setStatus(MyTestStatus.ASSIGNED_FOR_PREPARATION);
        MyTestPojo after = new MyTestPojo().setId(Long.valueOf("1002")).setName("pojo 2").setStatus(MyTestStatus.LOCKED_FOR_PREPARATION)
                .setAssignmentDate(TestUtils.parseDate("2020-01-25"))
                .setAvailabilityDate(TestUtils.parseDate("2020-01-20"));

        MyTestPojo actual = api.executeTransition(before, after);

        failBecauseExceptionWasNotThrown(ConfirmationRequiredException.class);
    }

    @Test
    public void testExecuteTransition6_ForceCheckConfirmed() {
        MyTestPojo before = new MyTestPojo().setId(Long.valueOf("1001")).setName("pojo 1").setStatus(MyTestStatus.ASSIGNED_FOR_PREPARATION);
        MyTestPojo after = new MyTestPojo().setId(Long.valueOf("1002")).setName("pojo 2").setStatus(MyTestStatus.LOCKED_FOR_PREPARATION)
                .setAssignmentDate(TestUtils.parseDate("2020-01-25"))
                .setAvailabilityDate(TestUtils.parseDate("2020-01-20"));

        MyTestPojo actual = api.executeTransition(before, after, true);

        assertThat(actual).isNotNull();
    }
}
