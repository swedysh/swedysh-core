package com.guenegom.swedysh.core.api.test;

import com.guenegom.swedysh.core.api.Swedysh;
import lombok.Data;

import java.util.Date;

@Data
public class MyTestPojo implements Swedysh {
    private Long id;
    private String name;
    private MyTestStatus status;
    private Integer mandatoryCount;
    private Date assignmentDate;
    private Date availabilityDate;

}
