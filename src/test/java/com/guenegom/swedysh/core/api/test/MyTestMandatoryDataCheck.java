package com.guenegom.swedysh.core.api.test;

import com.guenegom.swedysh.core.api.SwedyshCheck;
import com.guenegom.swedysh.core.exceptions.FailedCheckException;
import org.springframework.stereotype.Component;

@Component
public class MyTestMandatoryDataCheck implements SwedyshCheck<MyTestPojo> {
    @Override
    public void check(MyTestPojo source, MyTestPojo target) throws FailedCheckException {
        if (null == target.getMandatoryCount()) {
            throw new FailedCheckException("Count parameter is mandatory");
        }
    }
}
