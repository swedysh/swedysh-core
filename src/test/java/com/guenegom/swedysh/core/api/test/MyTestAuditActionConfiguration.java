package com.guenegom.swedysh.core.api.test;

import com.guenegom.swedysh.core.api.SwedyshAction;
import com.guenegom.swedysh.core.api.SwedyshCheck;
import com.guenegom.swedysh.core.exceptions.FailedCheckException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.Calendar;

@Configuration
@Slf4j
public class MyTestAuditActionConfiguration {

    @Bean
    SwedyshAction<MyTestPojo> myTestAssignmentAuditAction() {
        return (target) -> {
            target.setAssignmentDate(Calendar.getInstance().getTime());
            return target;
        };
    }

    @Bean
    SwedyshAction<MyTestPojo> myTestReadyAuditAction() {
        return (target) -> {
            target.setAvailabilityDate(Calendar.getInstance().getTime());
            return target;
        };
    }

    @Bean
    SwedyshCheck<MyTestPojo> myTestAllAssignedCheck() {
        return (s, t) -> {
            LOG.info("Checking the entities...");
        };
    }

    @Bean
    SwedyshCheck<MyTestPojo> myTestDatesConsistencyCheck() {
        return (s, t) -> {
            if (t.getAssignmentDate().after(t.getAvailabilityDate()))
                throw new FailedCheckException("Assignment must happen before disposal");
        };
    }

}
