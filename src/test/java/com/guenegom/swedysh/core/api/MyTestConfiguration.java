package com.guenegom.swedysh.core.api;

import com.guenegom.swedysh.core.SwedyshCore;
import com.guenegom.swedysh.core.api.test.MyTestPojo;
import com.guenegom.swedysh.core.workflow.SwedyshYamlBasedWorkflow;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.core.io.Resource;

import java.io.IOException;

@Configuration
@Import(SwedyshCore.class)
@ComponentScan(value = "com.rte.infoliaison.commons.swedysh.api.test")
public class MyTestConfiguration {

    @Value("classpath:/com/guenegom/swedysh/core/api/test/mytest.workflow.yml")
    private Resource myTestWorkflow;

    @Bean
    WorkflowApi<MyTestPojo> myWorkflow() throws IOException {
        return new SwedyshYamlBasedWorkflow<>(myTestWorkflow);
    }
}
