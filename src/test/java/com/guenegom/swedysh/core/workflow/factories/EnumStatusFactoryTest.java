package com.guenegom.swedysh.core.workflow.factories;

import com.guenegom.swedysh.core.api.test.MyTestStatus;
import com.guenegom.swedysh.test.utils.TestUtils;
import org.junit.Test;

import java.lang.reflect.InvocationTargetException;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.failBecauseExceptionWasNotThrown;

public class EnumStatusFactoryTest {

    @Test
    public void testConstruct1_Ok() throws NoSuchMethodException, IllegalAccessException, InvocationTargetException {
        EnumStatusFactory<MyTestStatus> actual = new EnumStatusFactory<MyTestStatus>(MyTestStatus.class);

        assertThat(actual).isNotNull();
        MyTestStatus[] actualValues = TestUtils.getPrivateField(actual, "values", MyTestStatus[].class);
        assertThat(actualValues).isNotEmpty().containsExactlyInAnyOrder(MyTestStatus.values());
    }

    @Test(expected = IllegalArgumentException.class)
    public void testConstruct2_Ko() throws NoSuchMethodException, IllegalAccessException, InvocationTargetException {
        EnumStatusFactory<StatusFactoryResolverTest.MyOtherTestStatus> actual = new EnumStatusFactory<StatusFactoryResolverTest.MyOtherTestStatus>(StatusFactoryResolverTest.MyOtherTestStatus.class);

        failBecauseExceptionWasNotThrown(IllegalArgumentException.class);
    }

    @Test
    public void testInstantiate1_Ok() throws NoSuchMethodException, IllegalAccessException, InvocationTargetException {
        EnumStatusFactory<MyTestStatus> factory = new EnumStatusFactory<MyTestStatus>(MyTestStatus.class);
        assertThat(factory).isNotNull();

        MyTestStatus actual = factory.instantiate(MyTestStatus.TO_EXECUTE.getCode());
        assertThat(actual).isSameAs(MyTestStatus.TO_EXECUTE);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testInstantiate2_Ko() throws NoSuchMethodException, IllegalAccessException, InvocationTargetException {
        EnumStatusFactory<MyTestStatus> factory = new EnumStatusFactory<MyTestStatus>(MyTestStatus.class);
        assertThat(factory).isNotNull();

        MyTestStatus actual = factory.instantiate("LOCKED");

        failBecauseExceptionWasNotThrown(IllegalArgumentException.class);
    }
}