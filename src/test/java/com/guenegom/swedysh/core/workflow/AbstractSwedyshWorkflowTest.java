package com.guenegom.swedysh.core.workflow;

import com.guenegom.swedysh.core.api.SwedyshStatus;
import com.guenegom.swedysh.core.api.SwedyshStatusFactory;
import com.guenegom.swedysh.core.api.test.MyTestPojo;
import com.guenegom.swedysh.core.api.test.MyTestStatus;
import com.guenegom.swedysh.core.exceptions.WorkflowConfigurationException;
import com.guenegom.swedysh.core.model.SwedyshTransitionConfiguration;
import com.guenegom.swedysh.core.model.SwedyshWorkflow;
import com.guenegom.swedysh.core.workflow.beans.ActualTransition;
import com.guenegom.swedysh.core.workflow.factories.StatusFactoryResolver;
import com.guenegom.swedysh.core.workflow.triggers.SimpleActionsTrigger;
import com.guenegom.swedysh.core.workflow.validators.SimpleValidator;
import com.guenegom.swedysh.test.utils.MyUnitTestConfiguration;
import com.guenegom.swedysh.test.utils.TestUtils;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.*;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = {MyUnitTestConfiguration.class, AbstractSwedyshWorkflow.class})
public class AbstractSwedyshWorkflowTest {

    @Autowired
    private AbstractSwedyshWorkflow<MyTestPojo> api;

    @MockBean
    private SimpleValidator validator;
    @MockBean
    private SimpleActionsTrigger<MyTestPojo> trigger;
    @MockBean
    private WorkflowPostConstructor postConstructor;
    @MockBean
    private StatusFactoryResolver statusFactoryResolver;

    @Before
    public void setup() {
        SwedyshWorkflow workflow = TestUtils.getPrivateField(api, "workflow", SwedyshWorkflow.class);
        WorkflowPostConstructor.setLevel1Defaults(workflow.getGlobals().getDefaults());
        workflow.getTransitions().forEach(t -> {
            WorkflowPostConstructor.setLevel2Defaults(workflow.getGlobals().getDefaults(), t);
        });
    }

    @Test
    public void testExecuteTransition() {
        MyTestPojo current = new MyTestPojo().setId(Long.valueOf("56")).setName("Pojo 56").setStatus(MyTestStatus.TO_PREPARE);
        MyTestPojo target = new MyTestPojo().setId(Long.valueOf("56")).setName("Pojo 56 v2").setStatus(MyTestStatus.READY);

        doNothing().when(validator).validate(any(ActualTransition.class), eq(false));
        when(trigger.trigger(any(ActualTransition.class))).thenReturn(target);
        MyTestPojo actual = api.executeTransition(current, target);

        assertThat(actual).isNotNull();
        assertThat(actual.getStatus()).isEqualTo(target.getStatus());
        verify(validator).validate(any(ActualTransition.class), eq(false));
    }

    @Test
    public void testGetCandidates1_TwoExpected() {
        MyTestPojo current = new MyTestPojo().setId(Long.valueOf("56")).setName("Pojo 56").setStatus(MyTestStatus.TO_PREPARE);
        SwedyshStatusFactory factory = new TestStatusFactory();

        when(statusFactoryResolver.resolveFactory(null, current.getStatus().getClass())).thenReturn(factory);
        List<SwedyshStatus> actual = api.getCandidates(current);

        assertThat(actual).isNotNull();
        assertThat(actual).containsExactly(MyTestStatus.ASSIGNED_FOR_PREPARATION, MyTestStatus.READY);
    }

    @Test
    public void testGetCandidates2_NoneExpected() {
        MyTestPojo current = new MyTestPojo().setId(Long.valueOf("56")).setName("Pojo 56").setStatus(MyTestStatus.LOCKED_FOR_EXECUTION);
        SwedyshStatusFactory factory = new TestStatusFactory();

        when(statusFactoryResolver.resolveFactory(null, current.getStatus().getClass())).thenReturn(factory);
        List<SwedyshStatus> actual = api.getCandidates(current);

        assertThat(actual)
                .isNotNull()
                .isEmpty();
    }

    private class TestStatusFactory implements SwedyshStatusFactory {
        @Override
        public MyTestStatus instantiate(String code) {
            return MyTestStatus.valueOf(code);
        }
    }

    @Test
    public void testFindMatchingTransition1_Ok() {
        MyTestStatus current = MyTestStatus.TO_PREPARE;
        MyTestStatus target = MyTestStatus.READY;

        SwedyshTransitionConfiguration actual = api.findMatchingTransition(current, target);

        assertThat(actual).isNotNull();
        assertThat(actual.getSource()).isEqualTo(current.getCode());
        assertThat(actual.getTarget()).isEqualTo(target.getCode());
    }

    @Test
    public void testFindMatchingTransition2_NotFound() {
        SwedyshTransitionConfiguration actual = api.findMatchingTransition(MyTestStatus.TO_PREPARE, MyTestStatus.TO_EXECUTE);
        assertThat(actual).isNull();
    }

    @Test(expected = WorkflowConfigurationException.class)
    public void testFindMatchingTransition3_TooMany() {
        SwedyshTransitionConfiguration actual = api.findMatchingTransition(MyTestStatus.READY, MyTestStatus.TO_EXECUTE);
    }
}
