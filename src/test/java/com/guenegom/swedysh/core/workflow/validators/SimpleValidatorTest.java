package com.guenegom.swedysh.core.workflow.validators;

import com.guenegom.swedysh.core.api.SwedyshCheck;
import com.guenegom.swedysh.core.api.test.MyTestPojo;
import com.guenegom.swedysh.core.api.test.MyTestStatus;
import com.guenegom.swedysh.core.exceptions.ConfirmationRequiredException;
import com.guenegom.swedysh.core.exceptions.FailedCheckException;
import com.guenegom.swedysh.core.exceptions.InvalidTransitionException;
import com.guenegom.swedysh.core.model.SwedyshCheckConfiguration;
import com.guenegom.swedysh.core.model.SwedyshTransitionConfiguration;
import com.guenegom.swedysh.core.workflow.beans.ActualTransition;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Arrays;
import java.util.Collections;

import static org.assertj.core.api.Assertions.failBecauseExceptionWasNotThrown;
import static org.mockito.Mockito.*;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = {SimpleValidator.class})
public class SimpleValidatorTest {

    private static final String CHECK_BEAN_NAME_1 = "myTestCheckBean1";
    private static final String CHECK_BEAN_NAME_2 = "myTestCheckBean2";

    @Autowired
    private SimpleValidator validator;

    @Mock
    private SwedyshCheck checkBean1;
    @Mock
    private SwedyshCheck checkBean2;

    @Test
    public void testValidate01_OkOneCheckPasses() throws FailedCheckException {
        MyTestPojo expectedSource = new MyTestPojo().setId(1l).setStatus(MyTestStatus.READY);
        MyTestPojo expectedTarget = new MyTestPojo().setId(1l).setStatus(MyTestStatus.TO_EXECUTE);
        SwedyshTransitionConfiguration expectedConfiguration = new SwedyshTransitionConfiguration()
                .setChecks(Collections.singletonList(new SwedyshCheckConfiguration().setId(CHECK_BEAN_NAME_1)))
                .setCheckBeans(Collections.singletonMap(CHECK_BEAN_NAME_1, checkBean1));

        ActualTransition<MyTestPojo> transition = new ActualTransition<>(expectedSource, expectedTarget, expectedConfiguration);

        validator.validate(transition);

        verify(checkBean1).check(expectedSource, expectedTarget);
    }

    @Test
    public void testValidate02_OkTwoCheckPass() throws FailedCheckException {
        MyTestPojo expectedSource = new MyTestPojo().setId(1l).setStatus(MyTestStatus.READY);
        MyTestPojo expectedTarget = new MyTestPojo().setId(1l).setStatus(MyTestStatus.TO_EXECUTE);
        SwedyshTransitionConfiguration expectedConfiguration = new SwedyshTransitionConfiguration()
                .setChecks(Arrays.asList(
                        new SwedyshCheckConfiguration().setId(CHECK_BEAN_NAME_1),
                        new SwedyshCheckConfiguration().setId(CHECK_BEAN_NAME_2)));
        expectedConfiguration.getCheckBeans().put(CHECK_BEAN_NAME_1, checkBean1);
        expectedConfiguration.getCheckBeans().put(CHECK_BEAN_NAME_2, checkBean2);

        ActualTransition<MyTestPojo> transition = new ActualTransition<>(expectedSource, expectedTarget, expectedConfiguration);

        validator.validate(transition);

        verify(checkBean1).check(expectedSource, expectedTarget);
        verify(checkBean2).check(expectedSource, expectedTarget);
    }

    @Test(expected = InvalidTransitionException.class)
    public void testValidate03_KoNotConfigured() {
        MyTestPojo expectedSource = new MyTestPojo().setId(1l).setStatus(MyTestStatus.READY);
        MyTestPojo expectedTarget = new MyTestPojo().setId(1l).setStatus(MyTestStatus.TO_EXECUTE);

        ActualTransition<MyTestPojo> transition = new ActualTransition<>(expectedSource, expectedTarget, null);

        validator.validate(transition);

        failBecauseExceptionWasNotThrown(InvalidTransitionException.class);
    }

    @Test(expected = InvalidTransitionException.class)
    public void testValidate04_KoOneOfOneCheckFails() throws FailedCheckException {
        MyTestPojo expectedSource = new MyTestPojo().setId(1l).setStatus(MyTestStatus.READY);
        MyTestPojo expectedTarget = new MyTestPojo().setId(1l).setStatus(MyTestStatus.TO_EXECUTE);
        SwedyshTransitionConfiguration expectedConfiguration = new SwedyshTransitionConfiguration()
                .setChecks(Collections.singletonList(new SwedyshCheckConfiguration().setId(CHECK_BEAN_NAME_1).setCanForce(Boolean.FALSE)))
                .setCheckBeans(Collections.singletonMap(CHECK_BEAN_NAME_1, checkBean1));

        doThrow(new FailedCheckException("Failure by unit test 4.1")).when(checkBean1).check(expectedSource, expectedTarget);

        ActualTransition<MyTestPojo> transition = new ActualTransition<>(expectedSource, expectedTarget, expectedConfiguration);

        validator.validate(transition);

        failBecauseExceptionWasNotThrown(InvalidTransitionException.class);
    }

    @Test(expected = InvalidTransitionException.class)
    public void testValidate05_KoOneOfTwoChecksFails() throws FailedCheckException {
        MyTestPojo expectedSource = new MyTestPojo().setId(1l).setStatus(MyTestStatus.READY);
        MyTestPojo expectedTarget = new MyTestPojo().setId(1l).setStatus(MyTestStatus.TO_EXECUTE);
        SwedyshTransitionConfiguration expectedConfiguration = new SwedyshTransitionConfiguration()
                .setChecks(Arrays.asList(
                        new SwedyshCheckConfiguration().setId(CHECK_BEAN_NAME_1).setCanForce(Boolean.FALSE),
                        new SwedyshCheckConfiguration().setId(CHECK_BEAN_NAME_2).setCanForce(Boolean.FALSE)));
        expectedConfiguration.getCheckBeans().put(CHECK_BEAN_NAME_1, checkBean1);
        expectedConfiguration.getCheckBeans().put(CHECK_BEAN_NAME_2, checkBean2);

        doThrow(new FailedCheckException("Failure by unit test 5.1")).when(checkBean1).check(expectedSource, expectedTarget);

        ActualTransition<MyTestPojo> transition = new ActualTransition<>(expectedSource, expectedTarget, expectedConfiguration);

        try {
            validator.validate(transition);
        } finally {
            verify(checkBean1).check(expectedSource, expectedTarget);
            verify(checkBean2, times(0)).check(expectedSource, expectedTarget);
        }
        failBecauseExceptionWasNotThrown(InvalidTransitionException.class);
    }

    @Test(expected = ConfirmationRequiredException.class)
    public void testValidate06_KoOneOfOneRequiresForce() throws FailedCheckException {
        MyTestPojo expectedSource = new MyTestPojo().setId(1l).setStatus(MyTestStatus.READY);
        MyTestPojo expectedTarget = new MyTestPojo().setId(1l).setStatus(MyTestStatus.TO_EXECUTE);
        SwedyshTransitionConfiguration expectedConfiguration = new SwedyshTransitionConfiguration()
                .setChecks(Collections.singletonList(new SwedyshCheckConfiguration().setId(CHECK_BEAN_NAME_1).setCanForce(Boolean.TRUE)))
                .setCheckBeans(Collections.singletonMap(CHECK_BEAN_NAME_1, checkBean1));

        doThrow(new FailedCheckException("Failure by unit test 6.1")).when(checkBean1).check(expectedSource, expectedTarget);

        ActualTransition<MyTestPojo> transition = new ActualTransition<>(expectedSource, expectedTarget, expectedConfiguration);

        validator.validate(transition);

        failBecauseExceptionWasNotThrown(InvalidTransitionException.class);
    }

    @Test
    public void testValidate07_OkOneOfOneRequiresForceAndIsForced() throws FailedCheckException {
        MyTestPojo expectedSource = new MyTestPojo().setId(1l).setStatus(MyTestStatus.READY);
        MyTestPojo expectedTarget = new MyTestPojo().setId(1l).setStatus(MyTestStatus.TO_EXECUTE);
        SwedyshTransitionConfiguration expectedConfiguration = new SwedyshTransitionConfiguration()
                .setChecks(Collections.singletonList(new SwedyshCheckConfiguration().setId(CHECK_BEAN_NAME_1).setCanForce(Boolean.TRUE)))
                .setCheckBeans(Collections.singletonMap(CHECK_BEAN_NAME_1, checkBean1));

        doThrow(new FailedCheckException("Failure by unit test 7.1")).when(checkBean1).check(expectedSource, expectedTarget);

        ActualTransition<MyTestPojo> transition = new ActualTransition<>(expectedSource, expectedTarget, expectedConfiguration);

        validator.validate(transition, true);

        verify(checkBean1).check(expectedSource, expectedTarget);
    }

    @Test(expected = ConfirmationRequiredException.class)
    public void testValidate08_KoOneOfTwoRequiresForce() throws FailedCheckException {
        MyTestPojo expectedSource = new MyTestPojo().setId(1l).setStatus(MyTestStatus.READY);
        MyTestPojo expectedTarget = new MyTestPojo().setId(1l).setStatus(MyTestStatus.TO_EXECUTE);
        SwedyshTransitionConfiguration expectedConfiguration = new SwedyshTransitionConfiguration()
                .setChecks(Arrays.asList(
                        new SwedyshCheckConfiguration().setId(CHECK_BEAN_NAME_1).setCanForce(Boolean.TRUE),
                        new SwedyshCheckConfiguration().setId(CHECK_BEAN_NAME_2).setCanForce(Boolean.FALSE)));
        expectedConfiguration.getCheckBeans().put(CHECK_BEAN_NAME_1, checkBean1);
        expectedConfiguration.getCheckBeans().put(CHECK_BEAN_NAME_2, checkBean2);

        doThrow(new FailedCheckException("Failure by unit test 8.1")).when(checkBean1).check(expectedSource, expectedTarget);

        ActualTransition<MyTestPojo> transition = new ActualTransition<>(expectedSource, expectedTarget, expectedConfiguration);

        validator.validate(transition);

        failBecauseExceptionWasNotThrown(InvalidTransitionException.class);
    }

    @Test
    public void testValidate09_OkOneOfTwoRequiresForceAndIsForced() throws FailedCheckException {
        MyTestPojo expectedSource = new MyTestPojo().setId(1l).setStatus(MyTestStatus.READY);
        MyTestPojo expectedTarget = new MyTestPojo().setId(1l).setStatus(MyTestStatus.TO_EXECUTE);
        SwedyshTransitionConfiguration expectedConfiguration = new SwedyshTransitionConfiguration()
                .setChecks(Arrays.asList(
                        new SwedyshCheckConfiguration().setId(CHECK_BEAN_NAME_1).setCanForce(Boolean.TRUE),
                        new SwedyshCheckConfiguration().setId(CHECK_BEAN_NAME_2).setCanForce(Boolean.FALSE)));
        expectedConfiguration.getCheckBeans().put(CHECK_BEAN_NAME_1, checkBean1);
        expectedConfiguration.getCheckBeans().put(CHECK_BEAN_NAME_2, checkBean2);

        doThrow(new FailedCheckException("Failure by unit test 9.1")).when(checkBean1).check(expectedSource, expectedTarget);

        ActualTransition<MyTestPojo> transition = new ActualTransition<>(expectedSource, expectedTarget, expectedConfiguration);

        validator.validate(transition, true);

        verify(checkBean1).check(expectedSource, expectedTarget);
        verify(checkBean2).check(expectedSource, expectedTarget);
    }

    @Test(expected = InvalidTransitionException.class)
    public void testValidate10_KoOneOfTwoRequiresForceAndIsForced() throws FailedCheckException {
        MyTestPojo expectedSource = new MyTestPojo().setId(1l).setStatus(MyTestStatus.READY);
        MyTestPojo expectedTarget = new MyTestPojo().setId(1l).setStatus(MyTestStatus.TO_EXECUTE);
        SwedyshTransitionConfiguration expectedConfiguration = new SwedyshTransitionConfiguration()
                .setChecks(Arrays.asList(
                        new SwedyshCheckConfiguration().setId(CHECK_BEAN_NAME_1).setCanForce(Boolean.TRUE),
                        new SwedyshCheckConfiguration().setId(CHECK_BEAN_NAME_2).setCanForce(Boolean.FALSE)));
        expectedConfiguration.getCheckBeans().put(CHECK_BEAN_NAME_1, checkBean1);
        expectedConfiguration.getCheckBeans().put(CHECK_BEAN_NAME_2, checkBean2);

        doThrow(new FailedCheckException("Failure by unit test 10.1")).when(checkBean1).check(expectedSource, expectedTarget);
        doThrow(new FailedCheckException("Failure by unit test 10.2")).when(checkBean2).check(expectedSource, expectedTarget);

        ActualTransition<MyTestPojo> transition = new ActualTransition<>(expectedSource, expectedTarget, expectedConfiguration);

        validator.validate(transition, true);

        try {
            validator.validate(transition);
        } finally {
            verify(checkBean1).check(expectedSource, expectedTarget);
            verify(checkBean2, times(0)).check(expectedSource, expectedTarget);
        }
        failBecauseExceptionWasNotThrown(InvalidTransitionException.class);
    }
}
