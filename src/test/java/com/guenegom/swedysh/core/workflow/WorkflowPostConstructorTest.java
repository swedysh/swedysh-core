package com.guenegom.swedysh.core.workflow;

import com.guenegom.swedysh.core.model.Level2DefaultConfiguration;
import com.guenegom.swedysh.core.model.SwedyshTransitionConfiguration;
import com.guenegom.swedysh.core.model.SwedyshWorkflow;
import com.guenegom.swedysh.core.model.enums.FailedActionStrategy;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.ApplicationContext;
import org.springframework.test.context.junit4.SpringRunner;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = {WorkflowPostConstructor.class})
public class WorkflowPostConstructorTest {

    @Autowired
    private WorkflowPostConstructor postConstructor;

    @MockBean
    private ApplicationContext applicationContext;

    @Test
    public void testProcess_DefaultLevel1() {
        Level2DefaultConfiguration actualDefaults = new Level2DefaultConfiguration();
        SwedyshWorkflow actualWorkflow = new SwedyshWorkflow();
        actualWorkflow.getGlobals().setDefaults(actualDefaults);

        assertThat(actualWorkflow.getGlobals().getDefaults().getDisplay()).isNull();
        assertThat(actualWorkflow.getGlobals().getDefaults().getCanForce()).isNull();
        assertThat(actualWorkflow.getGlobals().getDefaults().getOnFailedAction()).isNull();

        postConstructor.process(actualWorkflow);

        assertThat(actualWorkflow.getGlobals().getDefaults().getDisplay()).isEqualTo(SwedyshConstants.DEFAULT_DISPLAY);
        assertThat(actualWorkflow.getGlobals().getDefaults().getCanForce()).isEqualTo(SwedyshConstants.DEFAULT_CAN_FORCE_CHECK);
        assertThat(actualWorkflow.getGlobals().getDefaults().getOnFailedAction()).isEqualTo(SwedyshConstants.DEFAULT_FAILED_ACTION_STRATEGY);
    }

    @Test
    public void testProcess_DefaultLevel2() {
        Level2DefaultConfiguration expectedDefaults = new Level2DefaultConfiguration().setDisplay(Boolean.FALSE);
        expectedDefaults.setCanForce(Boolean.TRUE);
        expectedDefaults.setOnFailedAction(FailedActionStrategy.IGNORE);

        SwedyshWorkflow actualWorkflow = new SwedyshWorkflow();
        actualWorkflow.getGlobals().setDefaults(expectedDefaults);
        actualWorkflow.getTransitions().add(new SwedyshTransitionConfiguration().setSource("STATUS_1").setTarget("STATUS_2"));

        actualWorkflow.getTransitions().forEach(t -> {
            assertThat(t.getDisplay()).isNull();
            assertThat(t.getCanForce()).isNull();
            assertThat(t.getOnFailedAction()).isNull();
        });

        postConstructor.process(actualWorkflow);

        actualWorkflow.getTransitions().forEach(t -> assertThat(t.getDisplay()).isEqualTo(expectedDefaults.getDisplay()));
        actualWorkflow.getTransitions().forEach(t -> assertThat(t.getCanForce()).isEqualTo(expectedDefaults.getCanForce()));
        actualWorkflow.getTransitions().forEach(t -> assertThat(t.getOnFailedAction()).isEqualTo(expectedDefaults.getOnFailedAction()));
    }

    @Test
    public void testProcess_DefaultLevel3() {
        Level2DefaultConfiguration expectedDefaults = new Level2DefaultConfiguration();

        SwedyshWorkflow actualWorkflow = new SwedyshWorkflow();
        actualWorkflow.getGlobals().setDefaults(expectedDefaults);
        actualWorkflow.getTransitions().add(new SwedyshTransitionConfiguration().setSource("STATUS_1").setTarget("STATUS_2"));
        actualWorkflow.getTransitions().add(new SwedyshTransitionConfiguration().setSource("STATUS_2").setTarget("STATUS_3"));

        actualWorkflow.getTransitions().forEach(t -> {
            t.setDisplay(Boolean.FALSE);
            t.setCanForce(Boolean.TRUE);
        });

        assertThat(actualWorkflow.getGlobals().getDefaults().getDisplay()).isNull();
        assertThat(actualWorkflow.getGlobals().getDefaults().getCanForce()).isNull();
        assertThat(actualWorkflow.getGlobals().getDefaults().getOnFailedAction()).isNull();
        for (SwedyshTransitionConfiguration tr : actualWorkflow.getTransitions()) {
            tr.getChecks().forEach(t -> {
                assertThat(t.getCanForce()).isNull();
                assertThat(t.getOnFailedCheck()).isNull();
            });
            tr.getActions().forEach(t -> {
                assertThat(t.getOnFailedAction()).isNull();
            });
        }

        postConstructor.process(actualWorkflow);

        for (SwedyshTransitionConfiguration tr : actualWorkflow.getTransitions()) {
            tr.getChecks().forEach(t -> {
                assertThat(t.getCanForce()).isEqualTo(tr.getCanForce());
                assertThat(t.getOnFailedCheck()).isEqualTo(SwedyshConstants.DEFAULT_FAILED_CHECK_STRATEGY);
            });
            tr.getActions().forEach(t -> {
                assertThat(t.getOnFailedAction()).isEqualTo(SwedyshConstants.DEFAULT_FAILED_ACTION_STRATEGY);
            });
        }
    }
}
