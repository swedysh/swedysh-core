package com.guenegom.swedysh.core.workflow.triggers;

import com.guenegom.swedysh.core.api.SwedyshAction;
import com.guenegom.swedysh.core.api.test.MyTestPojo;
import com.guenegom.swedysh.core.api.test.MyTestStatus;
import com.guenegom.swedysh.core.model.SwedyshActionConfiguration;
import com.guenegom.swedysh.core.model.SwedyshTransitionConfiguration;
import com.guenegom.swedysh.core.workflow.beans.ActualTransition;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Collections;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.*;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = {SimpleActionsTrigger.class})
public class SimpleActionsTriggerTest {

    private static final String ACTION_BEAN_NAME = "myTestActionBean";

    @Autowired
    private SimpleActionsTrigger<MyTestPojo> trigger;

    @MockBean
    private SwedyshAction triggeredAction;

    @Test
    public void testTrigger_Ok() {
        MyTestPojo expectedSource = new MyTestPojo().setId(1l).setStatus(MyTestStatus.READY);
        MyTestPojo expectedTarget = new MyTestPojo().setId(1l).setStatus(MyTestStatus.TO_EXECUTE);
        SwedyshTransitionConfiguration expectedConfiguration = new SwedyshTransitionConfiguration()
                .setActions(Collections.singletonList(new SwedyshActionConfiguration().setId(ACTION_BEAN_NAME)))
                .setActionBeans(Collections.singletonMap(ACTION_BEAN_NAME, triggeredAction));

        int expectedCount = 56;
        when(triggeredAction.execute(expectedTarget)).thenReturn(expectedTarget.setMandatoryCount(expectedCount));

        ActualTransition<MyTestPojo> transition = new ActualTransition<>(expectedSource, expectedTarget, expectedConfiguration);

        MyTestPojo actual = trigger.trigger(transition);

        assertThat(actual).isNotNull().isEqualTo(expectedTarget);
        assertThat(actual.getMandatoryCount()).isEqualTo(expectedCount);
        verify(triggeredAction).execute(expectedTarget);
    }

    @Test
    public void testTrigger_None() {
        MyTestPojo expectedSource = new MyTestPojo().setId(1l).setStatus(MyTestStatus.READY);
        MyTestPojo expectedTarget = new MyTestPojo().setId(1l).setStatus(MyTestStatus.TO_EXECUTE);
        SwedyshTransitionConfiguration expectedConfiguration = new SwedyshTransitionConfiguration()
                .setActions(null)
                .setActionBeans(Collections.singletonMap(ACTION_BEAN_NAME, triggeredAction));

        ActualTransition<MyTestPojo> transition = new ActualTransition<>(expectedSource, expectedTarget, expectedConfiguration);

        MyTestPojo actual = trigger.trigger(transition);

        assertThat(actual).isNotNull().isEqualTo(expectedTarget);
        verify(triggeredAction, times(0)).execute(expectedTarget);
    }
}
