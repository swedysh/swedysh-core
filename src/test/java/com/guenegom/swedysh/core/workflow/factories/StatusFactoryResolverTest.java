package com.guenegom.swedysh.core.workflow.factories;

import com.guenegom.swedysh.core.api.SwedyshStatus;
import com.guenegom.swedysh.core.api.SwedyshStatusFactory;
import com.guenegom.swedysh.core.api.test.MyTestStatus;
import com.guenegom.swedysh.core.api.test.MyTestStatusFactory;
import com.guenegom.swedysh.core.exceptions.WorkflowConfigurationException;
import lombok.Data;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.springframework.beans.factory.NoUniqueBeanDefinitionException;
import org.springframework.context.ApplicationContext;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.failBecauseExceptionWasNotThrown;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(SpringRunner.class)
public class StatusFactoryResolverTest {

    private StatusFactoryResolver resolver;

    @Mock
    private ApplicationContext applicationContext;

    @Before
    public void setup() {
        resolver = new StatusFactoryResolver(this.applicationContext);
    }

    @Test
    public void testResolveFactory1_ConfiguredBeanName() {

        String expectedBean = "theConfiguredFactory";
        Class<MyTestStatus> expectedClass = MyTestStatus.class;

        when(applicationContext.getBean(expectedBean, SwedyshStatusFactory.class)).thenReturn(new MyTestStatusFactory());

        SwedyshStatusFactory actual = resolver.resolveFactory(expectedBean, expectedClass);

        assertThat(actual).isNotNull();
        verify(applicationContext).getBean(expectedBean, SwedyshStatusFactory.class);
    }

    @Test
    public void testResolveFactory2_ConfiguredBeanType() {

        Class<MyTestStatusFactory> expectedBean = MyTestStatusFactory.class;
        Class<MyTestStatus> expectedClass = MyTestStatus.class;

        when(applicationContext.getBean(expectedBean)).thenReturn(new MyTestStatusFactory());

        SwedyshStatusFactory actual = resolver.resolveFactory(expectedBean.getCanonicalName(), expectedClass);

        assertThat(actual)
                .isNotNull()
                .isInstanceOf(expectedBean);
        verify(applicationContext).getBean(expectedBean);
    }

    @Test
    public void testResolveFactory3_FactoryInContextOk() {

        Class<MyTestStatus> expectedClass = MyTestStatus.class;

        Map<String, SwedyshStatusFactory> map = new HashMap<>();
        map.put(SwedyshStatusFactory.class.getSimpleName(), new MyTestStatusFactory());

        when(applicationContext.getBeansOfType(SwedyshStatusFactory.class)).thenReturn(map);

        SwedyshStatusFactory actual = resolver.resolveFactory(null, expectedClass);

        assertThat(actual).isNotNull();
        verify(applicationContext).getBeansOfType(SwedyshStatusFactory.class);
    }

    @Test(expected = NoUniqueBeanDefinitionException.class)
    public void testResolveFactory4_FactoryInContextKo() {

        Class<MyTestStatus> expectedClass = MyTestStatus.class;

        Map<String, SwedyshStatusFactory> map = new HashMap<>();
        map.put(SwedyshStatusFactory.class.getSimpleName() + "1", new MyTestStatusFactory());
        map.put(SwedyshStatusFactory.class.getSimpleName() + "2", new MyTestStatusFactory());

        when(applicationContext.getBeansOfType(SwedyshStatusFactory.class)).thenReturn(map);

        SwedyshStatusFactory actual = resolver.resolveFactory(null, expectedClass);

        failBecauseExceptionWasNotThrown(NoUniqueBeanDefinitionException.class);
    }

    @Test
    public void testResolveFactory5_BuiltInEnumFactoryOk() {

        Class<MyTestStatus> expectedClass = MyTestStatus.class;

        when(applicationContext.getBeansOfType(SwedyshStatusFactory.class)).thenReturn(Collections.emptyMap());

        SwedyshStatusFactory actual = resolver.resolveFactory(null, expectedClass);

        assertThat(actual)
                .isNotNull()
                .isInstanceOf(EnumStatusFactory.class);
        verify(applicationContext).getBeansOfType(SwedyshStatusFactory.class);
    }

    @Test(expected = WorkflowConfigurationException.class)
    public void testResolveFactory6_KoNoFactoryAvailable() {

        when(applicationContext.getBeansOfType(SwedyshStatusFactory.class)).thenReturn(Collections.emptyMap());

        SwedyshStatusFactory actual = resolver.resolveFactory(null, MyOtherTestStatus.class);

        failBecauseExceptionWasNotThrown(WorkflowConfigurationException.class);
    }

    @Data
    static class MyOtherTestStatus implements SwedyshStatus {
        private Long id;
        private String code;
        private String label;
    }
}
