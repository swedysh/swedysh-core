package com.guenegom.swedysh.core.utils;

import com.guenegom.swedysh.core.api.test.MyTestMandatoryDataCheck;
import com.guenegom.swedysh.core.api.test.MyTestStatus;
import com.guenegom.swedysh.core.model.SwedyshTransitionConfiguration;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.ApplicationContext;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.ArrayList;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(SpringRunner.class)
public class SwedyshUtilsTest {

    @MockBean
    private ApplicationContext applicationContext;

    @Test
    public void testFindMatchingTransition1_Ok() {

        List<SwedyshTransitionConfiguration> expectedTransitions = getTestWorkflow();

        MyTestStatus expectedSource = MyTestStatus.TO_PREPARE;
        MyTestStatus expectedTarget = MyTestStatus.READY;

        List<SwedyshTransitionConfiguration> actual = SwedyshUtils.findMatchingTransitions(expectedTransitions, expectedSource, expectedTarget);
        assertThat(actual).isNotNull().hasSize(1);
    }

    @Test
    public void testFindMatchingTransition2_NoResult() {

        List<SwedyshTransitionConfiguration> expectedTransitions = getTestWorkflow();

        MyTestStatus expectedSource = MyTestStatus.TO_PREPARE;
        MyTestStatus expectedTarget = MyTestStatus.EXECUTED;

        List<SwedyshTransitionConfiguration> actual = SwedyshUtils.findMatchingTransitions(expectedTransitions, expectedSource, expectedTarget);
        assertThat(actual).isNotNull().isEmpty();
    }

    @Test
    public void testFindMatchingTransition3_Candidates() {

        List<SwedyshTransitionConfiguration> expectedTransitions = getTestWorkflow();

        MyTestStatus expectedSource = MyTestStatus.TO_PREPARE;

        List<SwedyshTransitionConfiguration> actual = SwedyshUtils.findMatchingTransitions(expectedTransitions, expectedSource, null);
        assertThat(actual).isNotNull().hasSize(2);
    }

    @Test
    public void testFindMatchingTransition4_NoCandidates() {

        List<SwedyshTransitionConfiguration> expectedTransitions = getTestWorkflow();

        MyTestStatus expectedSource = MyTestStatus.EXECUTED;

        List<SwedyshTransitionConfiguration> actual = SwedyshUtils.findMatchingTransitions(expectedTransitions, expectedSource, null);
        assertThat(actual).isNotNull().isEmpty();
    }

    @Test
    public void testGetBeanByNameOrType1_findByType() {
        String expectedBeanName = MyTestMandatoryDataCheck.class.getCanonicalName();
        Mockito.when(applicationContext.getBean(MyTestMandatoryDataCheck.class)).thenReturn(new MyTestMandatoryDataCheck());

        MyTestMandatoryDataCheck actual = SwedyshUtils.getBeanByNameOrType(applicationContext, expectedBeanName, MyTestMandatoryDataCheck.class);

        assertThat(actual).isNotNull();
    }

    @Test
    public void testGetBeanByNameOrType2_findByName() {
        String expectedBeanName = MyTestMandatoryDataCheck.class.getSimpleName().toLowerCase();
        Mockito.when(applicationContext.getBean(expectedBeanName, MyTestMandatoryDataCheck.class)).thenReturn(new MyTestMandatoryDataCheck());

        MyTestMandatoryDataCheck actual = SwedyshUtils.getBeanByNameOrType(applicationContext, expectedBeanName, MyTestMandatoryDataCheck.class);

        assertThat(actual).isNotNull();
    }

    private static List<SwedyshTransitionConfiguration> getTestWorkflow() {
        List<SwedyshTransitionConfiguration> expectedTransitions = new ArrayList<>();
        expectedTransitions.add(new SwedyshTransitionConfiguration().setSource(MyTestStatus.TO_PREPARE.name()).setTarget(MyTestStatus.ASSIGNED_FOR_PREPARATION.name()));
        expectedTransitions.add(new SwedyshTransitionConfiguration().setSource(MyTestStatus.ASSIGNED_FOR_PREPARATION.name()).setTarget(MyTestStatus.LOCKED_FOR_PREPARATION.name()));
        expectedTransitions.add(new SwedyshTransitionConfiguration().setSource(MyTestStatus.TO_PREPARE.name()).setTarget(MyTestStatus.READY.name()));
        expectedTransitions.add(new SwedyshTransitionConfiguration().setSource(MyTestStatus.LOCKED_FOR_PREPARATION.name()).setTarget(MyTestStatus.READY.name()));
        expectedTransitions.add(new SwedyshTransitionConfiguration().setSource(MyTestStatus.READY.name()).setTarget(MyTestStatus.TO_EXECUTE.name()));
        return expectedTransitions;
    }
}
