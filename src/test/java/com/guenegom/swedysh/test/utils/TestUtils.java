package com.guenegom.swedysh.test.utils;

import lombok.extern.slf4j.Slf4j;

import java.lang.reflect.Field;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

@Slf4j
public class TestUtils {
    private TestUtils() {
        // Static usage only
    }

    public static <T> T getPrivateField(Object object, String fieldName, Class<T> classOfField) {

        try {
            Field field = null;
            Class<?> cl = object.getClass();
            while (null == field && !Object.class.equals(cl)) {
                try {
                    LOG.info("Looking for field '{}' in class {}", fieldName, cl.getSimpleName());
                    field = cl.getDeclaredField(fieldName);
                } catch (NoSuchFieldException e) {
                    LOG.info("Did not find field '{}' in class {}", fieldName, cl.getSimpleName());
                    cl = cl.getSuperclass();
                }
            }
            if (null == field)
                throw new IllegalArgumentException(String.format("Unable to find field '%s' in class %s or one of its superclasses.", fieldName, object.getClass().getSimpleName()));

            field.setAccessible(true);
            return (T) field.get(object);
        } catch (SecurityException | IllegalAccessException e) {
            throw new IllegalArgumentException(e);
        }
    }

    public static Date parseDate(String theDate) {
        try {
            return new SimpleDateFormat("yyyy-MM-dd").parse(theDate);
        } catch (ParseException e) {
            throw new IllegalArgumentException(e);
        }
    }
}
