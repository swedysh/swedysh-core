package com.guenegom.swedysh.test.utils;

import com.guenegom.swedysh.core.api.WorkflowApi;
import com.guenegom.swedysh.core.api.test.MyTestPojo;
import com.guenegom.swedysh.core.workflow.SwedyshYamlBasedWorkflow;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.Resource;

import java.io.IOException;

@Configuration
public class MyUnitTestConfiguration {

    @Value("classpath:/com/guenegom/swedysh/core/workflow/unittest.workflow.yml")
    private Resource myTestWorkflow;

    @Bean
    public WorkflowApi<MyTestPojo> myWorkflow() throws IOException {
        return new SwedyshYamlBasedWorkflow<>(myTestWorkflow);
    }
}
