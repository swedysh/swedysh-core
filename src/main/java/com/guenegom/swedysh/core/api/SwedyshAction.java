package com.guenegom.swedysh.core.api;

public interface SwedyshAction<T extends Swedysh> {
    T execute(T toModify);
}
