package com.guenegom.swedysh.core.api;

public interface SwedyshStatusFactory<T extends SwedyshStatus> {
    T instantiate(String code);
}
