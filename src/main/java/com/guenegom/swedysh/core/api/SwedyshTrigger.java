package com.guenegom.swedysh.core.api;

import com.guenegom.swedysh.core.workflow.beans.ActualTransition;

public interface SwedyshTrigger<T extends Swedysh> {
    T trigger(ActualTransition<T> transition);
}
