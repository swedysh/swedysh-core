package com.guenegom.swedysh.core.api;

import com.guenegom.swedysh.core.workflow.beans.ActualTransition;

public interface SwedyshValidator<T extends Swedysh> {
    void validate(ActualTransition<T> transition, boolean force);

    default void validate(ActualTransition<T> transition) {
        validate(transition, false);
    }
}
