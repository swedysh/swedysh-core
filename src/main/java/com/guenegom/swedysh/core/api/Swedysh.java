package com.guenegom.swedysh.core.api;

public interface Swedysh {
    SwedyshStatus getStatus();
}
