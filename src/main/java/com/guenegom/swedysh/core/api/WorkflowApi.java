package com.guenegom.swedysh.core.api;

import java.util.List;

public interface WorkflowApi<T extends Swedysh> {

    default T executeTransition(T current, T target) {
        return executeTransition(current, target, false);
    }

    /**
     * Executes the transition from status {@code current.getStatus()} to {@code target.getStatus()}.<br/>
     * It includes:
     * - running the configured controls to validate that transition
     * - executing the actions triggered by that transition
     *
     * @param current The current representation of the {@link Swedysh} object with its origin status
     * @param target  The current or next representation of the {@link Swedysh} object with its target status
     * @param force   Force the checks to ignore the rejected transition if checked is marked 'force: allowed'
     * @return The new {@link Swedysh} object with modifications done by actions triggered
     */
    T executeTransition(T current, T target, boolean force);

    /**
     * Lists all the status that can be reached from {@code current.getStatus()}
     *
     * @param current The current representation of the {@link Swedysh} object with its origin status
     * @return
     */
    List<SwedyshStatus> getCandidates(T current);
}
