package com.guenegom.swedysh.core.api;

import com.guenegom.swedysh.core.exceptions.FailedCheckException;

public interface SwedyshCheck<T extends Swedysh> {
    void check(T source, T target) throws FailedCheckException;
}
