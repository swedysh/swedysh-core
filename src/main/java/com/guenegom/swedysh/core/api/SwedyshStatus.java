package com.guenegom.swedysh.core.api;

/**
 * It is highly recommended to implement the status as an enum, or at least, have a constructor that takes the code as input.
 */
public interface SwedyshStatus {
    String getCode();
}
