package com.guenegom.swedysh.core.utils;

import com.guenegom.swedysh.core.api.SwedyshStatus;
import com.guenegom.swedysh.core.model.SwedyshTransitionConfiguration;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.ApplicationContext;
import org.springframework.lang.NonNull;
import org.springframework.lang.Nullable;

import java.util.List;
import java.util.Objects;
import java.util.function.Predicate;
import java.util.stream.Collectors;

@Slf4j
public class SwedyshUtils {

    public static List<SwedyshTransitionConfiguration> findMatchingTransitions(@NonNull final List<SwedyshTransitionConfiguration> haystack, @NonNull final SwedyshStatus source, @Nullable final SwedyshStatus target) {
        Objects.requireNonNull(source);

        Predicate<SwedyshTransitionConfiguration> predicate = t -> {
            if (!source.getCode().equalsIgnoreCase(t.getSource())) {
                return false;
            }
            if (target != null && !target.getCode().equalsIgnoreCase(t.getTarget())) {
                return false;
            }
            return true;
        };
        List<SwedyshTransitionConfiguration> matches = haystack.stream().filter(predicate).collect(Collectors.toList());

        Objects.requireNonNull(matches);
        return matches;
    }

    public static <T> T getBeanByNameOrType(ApplicationContext applicationContext, String beanIdentifier, Class<T> type) {
        T bean;
        Class<?> clazz = null;
        try {
            clazz = Class.forName(beanIdentifier);
        } catch (ClassNotFoundException e) {
            LOG.warn("Bean identifier {} cannot represent a class type. Trying directly with a name", beanIdentifier);
        }

        if (clazz != null) {
            bean = (T) applicationContext.getBean(clazz);
        } else {
            bean = applicationContext.getBean(beanIdentifier, type);
        }
        return bean;
    }
}