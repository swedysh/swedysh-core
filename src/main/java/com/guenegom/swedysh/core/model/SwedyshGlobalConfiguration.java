package com.guenegom.swedysh.core.model;

import com.guenegom.swedysh.core.model.enums.StatusEqualityStrategy;
import lombok.Data;

@Data
public class SwedyshGlobalConfiguration {
    private String factory;
    private Level2DefaultConfiguration defaults = new Level2DefaultConfiguration();
    private StatusEqualityStrategy onSourceEqualsTarget;
}
