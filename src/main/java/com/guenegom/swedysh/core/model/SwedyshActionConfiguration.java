package com.guenegom.swedysh.core.model;

import com.guenegom.swedysh.core.model.enums.FailedActionStrategy;
import lombok.Data;

@Data
public class SwedyshActionConfiguration implements Level3ActionDefaultConfiguration {
    private String id;
    private FailedActionStrategy onFailedAction;

}
