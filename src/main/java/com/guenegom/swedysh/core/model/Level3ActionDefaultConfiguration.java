package com.guenegom.swedysh.core.model;

import com.guenegom.swedysh.core.model.enums.FailedActionStrategy;

public interface Level3ActionDefaultConfiguration {

    FailedActionStrategy getOnFailedAction();

    Level3ActionDefaultConfiguration setOnFailedAction(FailedActionStrategy onFailedAction);
}
