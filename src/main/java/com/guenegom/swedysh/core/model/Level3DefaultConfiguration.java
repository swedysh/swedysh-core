package com.guenegom.swedysh.core.model;

import com.guenegom.swedysh.core.model.enums.FailedActionStrategy;
import com.guenegom.swedysh.core.model.enums.FailedCheckStrategy;
import lombok.Data;

@Data
public class Level3DefaultConfiguration implements Level3ActionDefaultConfiguration, Level3CheckDefaultConfiguration {

    private FailedCheckStrategy onFailedCheck;
    private FailedActionStrategy onFailedAction;
    private Boolean canForce;
}
