package com.guenegom.swedysh.core.model;

import com.guenegom.swedysh.core.api.Swedysh;
import com.guenegom.swedysh.core.api.WorkflowApi;
import lombok.Data;

@Data
public class Level2DefaultConfiguration extends Level3DefaultConfiguration {
    /**
     * Display this transition when listing candidates?
     * If false, {@link WorkflowApi#getCandidates(Swedysh)} } will not show this transition;
     */
    private Boolean display;
}
