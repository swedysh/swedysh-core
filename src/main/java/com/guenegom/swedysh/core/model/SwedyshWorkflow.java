package com.guenegom.swedysh.core.model;

import lombok.Data;

import java.util.ArrayList;
import java.util.List;

@Data
public class SwedyshWorkflow {
    private SwedyshGlobalConfiguration globals = new SwedyshGlobalConfiguration();
    private List<SwedyshTransitionConfiguration> transitions = new ArrayList<>();
}
