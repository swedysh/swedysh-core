package com.guenegom.swedysh.core.model.enums;

public enum StatusEqualityStrategy {
    EXECUTE, IGNORE
}
