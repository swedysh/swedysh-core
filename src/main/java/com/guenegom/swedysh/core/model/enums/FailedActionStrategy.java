package com.guenegom.swedysh.core.model.enums;

public enum FailedActionStrategy {
    FAIL, IGNORE
}
