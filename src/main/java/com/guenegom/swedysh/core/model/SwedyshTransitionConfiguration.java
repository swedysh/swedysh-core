package com.guenegom.swedysh.core.model;

import com.guenegom.swedysh.core.api.SwedyshAction;
import com.guenegom.swedysh.core.api.SwedyshCheck;
import lombok.Data;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Data
public class SwedyshTransitionConfiguration extends Level2DefaultConfiguration {
    private String source;
    private String target;
    private List<SwedyshCheckConfiguration> checks = new ArrayList<>();
    private Map<String, SwedyshCheck> checkBeans = new HashMap<>();
    private List<SwedyshActionConfiguration> actions = new ArrayList<>();
    private Map<String, SwedyshAction> actionBeans = new HashMap<>();
}
