package com.guenegom.swedysh.core.model;

import com.guenegom.swedysh.core.model.enums.FailedCheckStrategy;
import lombok.Data;

@Data
public class SwedyshCheckConfiguration implements Level3CheckDefaultConfiguration {
    private String id;
    private FailedCheckStrategy onFailedCheck;
    private Boolean canForce;

}
