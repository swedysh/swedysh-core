package com.guenegom.swedysh.core.model.enums;

public enum FailedCheckStrategy {
    FAIL, CONTINUE, FORCE
}
