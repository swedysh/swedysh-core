package com.guenegom.swedysh.core.model;

import com.guenegom.swedysh.core.model.enums.FailedCheckStrategy;

public interface Level3CheckDefaultConfiguration {
    FailedCheckStrategy getOnFailedCheck();

    Level3CheckDefaultConfiguration setOnFailedCheck(FailedCheckStrategy onfailedCheck);

    Boolean getCanForce();

    Level3CheckDefaultConfiguration setCanForce(Boolean canForce);

}
