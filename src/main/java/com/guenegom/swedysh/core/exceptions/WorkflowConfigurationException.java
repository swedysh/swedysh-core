package com.guenegom.swedysh.core.exceptions;

public class WorkflowConfigurationException extends RuntimeException {

    public WorkflowConfigurationException(String s) {
        super(s);
    }

    public WorkflowConfigurationException(Throwable throwable) {
        super(throwable);
    }
}
