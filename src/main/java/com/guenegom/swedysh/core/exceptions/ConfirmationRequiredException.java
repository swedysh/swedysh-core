package com.guenegom.swedysh.core.exceptions;

public class ConfirmationRequiredException extends AbstractFailedTransitionException {
    public ConfirmationRequiredException(String s) {
        super(s);
    }

    public ConfirmationRequiredException(String s, Throwable... causes) {
        super(s, causes);
    }

    public ConfirmationRequiredException(Throwable... causes) {
        super(causes);
    }
}
