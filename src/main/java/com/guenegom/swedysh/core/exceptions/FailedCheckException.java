package com.guenegom.swedysh.core.exceptions;

public class FailedCheckException extends Exception {
    public FailedCheckException(String reason) {
        super(reason);
    }
}
