package com.guenegom.swedysh.core.exceptions;

public class InvalidTransitionException extends AbstractFailedTransitionException {
    public InvalidTransitionException(String s) {
        super(s);
    }

    public InvalidTransitionException(String s, FailedCheckException... causes) {
        super(s, causes);
    }

    public InvalidTransitionException(FailedCheckException... causes) {
        super(causes);
    }
}
