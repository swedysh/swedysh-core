package com.guenegom.swedysh.core.exceptions;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public abstract class AbstractFailedTransitionException extends RuntimeException {
    private List<? extends Throwable> causes = new ArrayList<>();

    public AbstractFailedTransitionException(String s) {
        super(s);
    }

    public AbstractFailedTransitionException(String s, Throwable... causes) {
        this(s);
        setCauses(causes);
    }

    public AbstractFailedTransitionException(Throwable... causes) {
        setCauses(causes);
    }

    private AbstractFailedTransitionException setCauses(Throwable... causes) {
        this.causes = Arrays.asList(causes);
        return this;
    }
}
