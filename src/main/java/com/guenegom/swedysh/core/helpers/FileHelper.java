package com.guenegom.swedysh.core.helpers;

import org.springframework.core.io.Resource;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Objects;

public class FileHelper {

    public static String readStringFromFile(Resource sourceFile) throws IOException {
        Objects.requireNonNull(sourceFile);
        byte[] encoded = Files.readAllBytes(Paths.get(sourceFile.getFile().getAbsolutePath()));
        String content = new String(encoded, StandardCharsets.UTF_8);
        return content;
    }
}
