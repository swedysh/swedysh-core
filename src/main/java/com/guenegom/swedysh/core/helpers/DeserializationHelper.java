package com.guenegom.swedysh.core.helpers;

import com.fasterxml.jackson.core.JsonFactory;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.*;
import com.fasterxml.jackson.databind.deser.BeanDeserializerModifier;
import com.fasterxml.jackson.databind.module.SimpleModule;
import com.fasterxml.jackson.dataformat.yaml.YAMLFactory;
import lombok.extern.java.Log;
import org.springframework.lang.Nullable;

import java.io.IOException;

@Log
public class DeserializationHelper {

    public static <T> T getJsonStringAsPojo(String json, Class<T> clazz, DeserializationFeature... additionalFeatures) throws IOException {

        ObjectMapper mapper = createObjectMapper(null, additionalFeatures);

        T result = mapper.readValue(json, clazz);
        return result;
    }

    public static <T> T getYamlStringAsPojo(String yaml, Class<T> clazz, DeserializationFeature... additionalFeatures) throws IOException {

        ObjectMapper mapper = createObjectMapper(new YAMLFactory(), additionalFeatures);

        T result = mapper.readValue(yaml, clazz);
        return result;
    }

    private static ObjectMapper createObjectMapper(@Nullable JsonFactory jf, DeserializationFeature[] additionalFeatures) {
        ObjectMapper mapper = new ObjectMapper(jf);
        mapper.configure(SerializationFeature.FAIL_ON_EMPTY_BEANS, false);
        mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
        mapper.registerModule(getModule());
        for (DeserializationFeature feature : additionalFeatures) {
            mapper.enable(feature);
        }
        return mapper;
    }

    private static Module getModule() {
        SimpleModule module = new SimpleModule();
        module.setDeserializerModifier(new BeanDeserializerModifier() {
            @Override
            public JsonDeserializer<?> modifyEnumDeserializer(DeserializationConfig config, JavaType type, BeanDescription beanDesc, JsonDeserializer<?> deserializer) {
                return new JsonDeserializer<Enum>() {
                    @Override
                    public Enum deserialize(JsonParser jp, DeserializationContext ctxt) throws IOException {
                        Class<? extends Enum> rawClass = (Class<Enum<?>>) type.getRawClass();
                        return Enum.valueOf(rawClass, jp.getValueAsString().toUpperCase());
                    }
                };
            }
        });
        return module;
    }
}
