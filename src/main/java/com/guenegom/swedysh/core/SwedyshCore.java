package com.guenegom.swedysh.core;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan("com.guenegom.swedysh.core")
public class SwedyshCore {
}
