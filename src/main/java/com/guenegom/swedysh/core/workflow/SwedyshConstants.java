package com.guenegom.swedysh.core.workflow;

import com.guenegom.swedysh.core.model.enums.FailedActionStrategy;
import com.guenegom.swedysh.core.model.enums.FailedCheckStrategy;

public interface SwedyshConstants {

    String ANY_JOKER = "ANY";

    Boolean DEFAULT_DISPLAY = Boolean.TRUE;
    Boolean DEFAULT_CAN_FORCE_CHECK = Boolean.FALSE;
    FailedCheckStrategy DEFAULT_FAILED_CHECK_STRATEGY = FailedCheckStrategy.FAIL;
    FailedActionStrategy DEFAULT_FAILED_ACTION_STRATEGY = FailedActionStrategy.FAIL;
}
