package com.guenegom.swedysh.core.workflow;

import com.guenegom.swedysh.core.api.Swedysh;
import com.guenegom.swedysh.core.helpers.DeserializationHelper;
import com.guenegom.swedysh.core.helpers.FileHelper;
import com.guenegom.swedysh.core.model.SwedyshWorkflow;
import org.springframework.core.io.Resource;

import java.io.IOException;

public class SwedyshYamlBasedWorkflow<T extends Swedysh> extends AbstractSwedyshWorkflow<T> {

    public SwedyshYamlBasedWorkflow(Resource yamlResource) throws IOException {
        super(DeserializationHelper.getYamlStringAsPojo(FileHelper.readStringFromFile(yamlResource), SwedyshWorkflow.class));
    }
}
