package com.guenegom.swedysh.core.workflow.beans;

import com.guenegom.swedysh.core.api.Swedysh;
import com.guenegom.swedysh.core.model.SwedyshTransitionConfiguration;
import lombok.Data;

@Data
public class ActualTransition<T extends Swedysh> {

    private final T source, target;
    private final SwedyshTransitionConfiguration configuredTransition;

    public ActualTransition(T source, T target, SwedyshTransitionConfiguration transition) {
        this.source = source;
        this.target = target;
        this.configuredTransition = transition;
    }
}
