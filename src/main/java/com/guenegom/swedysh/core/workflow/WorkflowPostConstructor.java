package com.guenegom.swedysh.core.workflow;

import com.guenegom.swedysh.core.api.SwedyshAction;
import com.guenegom.swedysh.core.api.SwedyshCheck;
import com.guenegom.swedysh.core.model.Level2DefaultConfiguration;
import com.guenegom.swedysh.core.model.SwedyshTransitionConfiguration;
import com.guenegom.swedysh.core.model.SwedyshWorkflow;
import com.guenegom.swedysh.core.utils.SwedyshUtils;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Component;

@Component
@Slf4j
@RequiredArgsConstructor
public class WorkflowPostConstructor {

    private final ApplicationContext applicationContext;

    void process(SwedyshWorkflow workflow) {
        setLevel1Defaults(workflow.getGlobals().getDefaults());
        workflow.getTransitions().forEach(t -> {
            setLevel2Defaults(workflow.getGlobals().getDefaults(), t);
            setLevel3Defaults(t);
            addCheckBeans(t);
            addActionBeans(t);
        });
    }

    static void setLevel1Defaults(Level2DefaultConfiguration defaults) {
        if (null == defaults.getDisplay()) defaults.setDisplay(SwedyshConstants.DEFAULT_DISPLAY);
        if (null == defaults.getOnFailedCheck()) defaults.setOnFailedCheck(SwedyshConstants.DEFAULT_FAILED_CHECK_STRATEGY);
        if (null == defaults.getCanForce()) defaults.setCanForce(SwedyshConstants.DEFAULT_CAN_FORCE_CHECK);
        if (null == defaults.getOnFailedAction()) defaults.setOnFailedAction(SwedyshConstants.DEFAULT_FAILED_ACTION_STRATEGY);
    }

    static void setLevel2Defaults(Level2DefaultConfiguration defaults, SwedyshTransitionConfiguration transition) {
        if (null == transition.getDisplay()) transition.setDisplay(defaults.getDisplay());
        if (null == transition.getOnFailedCheck()) transition.setOnFailedCheck(defaults.getOnFailedCheck());
        if (null == transition.getCanForce()) transition.setCanForce(defaults.getCanForce());
        if (null == transition.getOnFailedAction()) transition.setOnFailedAction(defaults.getOnFailedAction());
    }

    static void setLevel3Defaults(SwedyshTransitionConfiguration transition) {
        transition.getChecks().forEach(t -> {
            if (null == t.getCanForce()) t.setCanForce(transition.getCanForce());
            if (null == t.getOnFailedCheck()) t.setOnFailedCheck(transition.getOnFailedCheck());
        });
        transition.getActions().forEach(t -> {
            if (null == t.getOnFailedAction()) t.setOnFailedAction(transition.getOnFailedAction());
        });
    }

    private void addCheckBeans(SwedyshTransitionConfiguration transition) {
        transition.getChecks().forEach(t -> transition.getCheckBeans().put(t.getId(),
                SwedyshUtils.getBeanByNameOrType(applicationContext, t.getId(), SwedyshCheck.class)));
    }

    private void addActionBeans(SwedyshTransitionConfiguration transition) {
        transition.getActions().forEach(t -> transition.getActionBeans().put(t.getId(),
                SwedyshUtils.getBeanByNameOrType(applicationContext, t.getId(), SwedyshAction.class)));
    }

}
