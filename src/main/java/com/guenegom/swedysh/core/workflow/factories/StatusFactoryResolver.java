package com.guenegom.swedysh.core.workflow.factories;

import com.guenegom.swedysh.core.api.SwedyshStatus;
import com.guenegom.swedysh.core.api.SwedyshStatusFactory;
import com.guenegom.swedysh.core.exceptions.WorkflowConfigurationException;
import com.guenegom.swedysh.core.utils.SwedyshUtils;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.NoUniqueBeanDefinitionException;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Component;

import java.lang.reflect.InvocationTargetException;
import java.util.Map;

@Component
@Slf4j
@RequiredArgsConstructor
public class StatusFactoryResolver {

    private final ApplicationContext applicationContext;

    /**
     * Returns a status factory based on the following priorities
     * <ol>
     * <li>A factory was configured by user in workflow.yml under globals.factory key<br/>
     *     - If this configuration does not represent an existing bean, throws {@link org.springframework.beans.factory.NoSuchBeanDefinitionException}</li>
     * <li>A class that implements {@link SwedyshStatusFactory} is available in the applicationContext
     *     - If several beans are found, throws a {@link org.springframework.beans.factory.NoUniqueBeanDefinitionException}</li>
     * <li>Based on the type of the SwedyshStatus object itself:
     * <ol>
     *     <li>It is an enum: Use the builtin EnumStatusFactory</li>
     *     <li>It is a String: Use the simple builtin StringStatusFactory</li>
     * </ol>
     * </li>
     * <li>Otherwise: The case is not supported yet.</li>
     * </ol>
     *
     * @param configuredFactory The factory name or type configured under globals.factory key
     * @param typeOfStatus      The type of the implementation of the {@link SwedyshStatus} that we are currently dealing with
     * @return The factory
     */
    public SwedyshStatusFactory resolveFactory(String configuredFactory, Class<? extends SwedyshStatus> typeOfStatus) {
        if (StringUtils.isNotBlank(configuredFactory)) {
            return SwedyshUtils.getBeanByNameOrType(applicationContext, configuredFactory, SwedyshStatusFactory.class);
        }
        Map<String, SwedyshStatusFactory> beans = applicationContext.getBeansOfType(SwedyshStatusFactory.class);
        if (beans.size() > 1) {
            throw new NoUniqueBeanDefinitionException(SwedyshStatusFactory.class, beans.size(), "Expecting no more than 1 factory. Consider using 'globals.factory' key to explicitly declare which to use.");
        }
        if (beans.size() == 1) {
            return beans.values().stream().findFirst().get();
        }
        if (typeOfStatus.isEnum()) {
            return instantiateEnumFactory(typeOfStatus);
        }
        throw new WorkflowConfigurationException("No factory found for type " + typeOfStatus.getClass().getName());
    }

    private static SwedyshStatusFactory instantiateEnumFactory(Class<? extends SwedyshStatus> typeOfStatus) {
        try {
            return new EnumStatusFactory(typeOfStatus);
        } catch (NoSuchMethodException | IllegalAccessException | InvocationTargetException e) {
            throw new WorkflowConfigurationException(e);
        }
    }
}
