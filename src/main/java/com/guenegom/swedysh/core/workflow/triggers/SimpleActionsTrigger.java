package com.guenegom.swedysh.core.workflow.triggers;

import com.guenegom.swedysh.core.api.Swedysh;
import com.guenegom.swedysh.core.api.SwedyshAction;
import com.guenegom.swedysh.core.api.SwedyshTrigger;
import com.guenegom.swedysh.core.model.SwedyshActionConfiguration;
import com.guenegom.swedysh.core.workflow.beans.ActualTransition;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections4.CollectionUtils;
import org.springframework.stereotype.Component;

import java.util.Objects;

@Slf4j
@Component
public class SimpleActionsTrigger<T extends Swedysh> implements SwedyshTrigger<T> {

    @Override
    public T trigger(ActualTransition<T> transition) {

        Objects.requireNonNull(transition);
        Objects.requireNonNull(transition.getConfiguredTransition());

        if (CollectionUtils.isEmpty(transition.getConfiguredTransition().getActions())) {
            LOG.info("Transition from {} to {}: No action to trigger", transition.getSource().getStatus().getCode(), transition.getTarget().getStatus().getCode());
            return transition.getTarget();
        }

        return triggerActions(transition);
    }

    private T triggerActions(ActualTransition<T> transition) {
        T result = transition.getTarget();
        for (SwedyshActionConfiguration action : transition.getConfiguredTransition().getActions()) {
            SwedyshAction<T> actionBean = transition.getConfiguredTransition().getActionBeans().get(action.getId());

            LOG.info("Transition from {} to {}: Running action '{}'", transition.getSource().getStatus().getCode(), transition.getTarget().getStatus().getCode(), action.getId());

            result = actionBean.execute(result);
        }
        return result;
    }

}
