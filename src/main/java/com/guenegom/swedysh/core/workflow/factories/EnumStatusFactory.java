package com.guenegom.swedysh.core.workflow.factories;

import com.guenegom.swedysh.core.api.SwedyshStatus;
import com.guenegom.swedysh.core.api.SwedyshStatusFactory;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

public class EnumStatusFactory<T extends SwedyshStatus> implements SwedyshStatusFactory<T> {

    private final Class<T> className;
    private T[] values;

    public EnumStatusFactory(Class<T> className) throws NoSuchMethodException, IllegalAccessException, InvocationTargetException {
        if (!className.isEnum()) {
            throw new IllegalArgumentException("Enum expected");
        }
        this.className = className;
        init();
    }

    void init() throws NoSuchMethodException, InvocationTargetException, IllegalAccessException {
        Method method = className.getMethod("values");
        this.values = (T[]) method.invoke(null);
    }

    @Override
    public T instantiate(String code) {
        // If it works, load reflection-based stuff at startup
        for (T item : values) {
            if (item.getCode().equals(code)) {
                return item;
            }
        }
        throw new IllegalArgumentException(String.format("Unknown code '%s' for enum %s", code, this.className.getSimpleName()));
    }
}
