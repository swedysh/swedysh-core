package com.guenegom.swedysh.core.workflow.validators;

import com.guenegom.swedysh.core.api.Swedysh;
import com.guenegom.swedysh.core.api.SwedyshCheck;
import com.guenegom.swedysh.core.api.SwedyshValidator;
import com.guenegom.swedysh.core.exceptions.ConfirmationRequiredException;
import com.guenegom.swedysh.core.exceptions.FailedCheckException;
import com.guenegom.swedysh.core.exceptions.InvalidTransitionException;
import com.guenegom.swedysh.core.model.SwedyshCheckConfiguration;
import com.guenegom.swedysh.core.workflow.beans.ActualTransition;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections4.CollectionUtils;
import org.springframework.stereotype.Component;

import java.util.Arrays;
import java.util.Objects;

@Component
@Slf4j
@RequiredArgsConstructor
public class SimpleValidator<T extends Swedysh> implements SwedyshValidator<T> {

    private static final String REASONS = " Reasons: ";
    private static final String INDENT = "  * ";

    private void rejectTransition(ActualTransition<T> transition, FailedCheckException... causes) {
        String msg = String.format("Transition from %s to %s is not authorized.", transition.getSource().getStatus().getCode(), transition.getTarget().getStatus().getCode());
        LOG.warn(msg + REASONS);
        Arrays.stream(causes).forEach(t -> LOG.warn(INDENT + t.getMessage()));
        throw new InvalidTransitionException(msg, causes);
    }

    private void requireConfirmation(ActualTransition<T> transition, FailedCheckException... causes) {
        String msg = String.format("Transition from %s to %s is not authorized but can be forced. Consider re-executing transition with parameter 'force=true'.", transition.getSource().getStatus().getCode(), transition.getTarget().getStatus().getCode());
        LOG.warn(msg + REASONS);
        Arrays.stream(causes).forEach(t -> LOG.warn(INDENT + t.getMessage()));
        throw new ConfirmationRequiredException(msg, causes);
    }

    private void warnTransitionForced(ActualTransition<T> transition, FailedCheckException... causes) {
        String msg = String.format("Transition from %s to %s is not supposed to be authorized but was forced.", transition.getSource().getStatus().getCode(), transition.getTarget().getStatus().getCode());
        LOG.warn(msg + REASONS);
        Arrays.stream(causes).forEach(t -> LOG.warn(INDENT + t.getMessage()));
    }

    @Override
    public void validate(ActualTransition<T> transition, boolean force) {

        Objects.requireNonNull(transition);
        if (null == transition.getConfiguredTransition()) {
            rejectTransition(transition, new FailedCheckException("Transition is not configured"));
        }

        if (CollectionUtils.isNotEmpty(transition.getConfiguredTransition().getChecks())) {
            runChecks(transition, force);
        }

        LOG.info("Transition from {} to {} is authorized", transition.getSource().getStatus().getCode(), transition.getTarget().getStatus().getCode());
    }

    private void runChecks(ActualTransition<T> transition, boolean force) {
        for (SwedyshCheckConfiguration check : transition.getConfiguredTransition().getChecks()) {
            SwedyshCheck<T> checkBean = transition.getConfiguredTransition().getCheckBeans().get(check.getId());

            LOG.info("Testing transition from {} to {} : Running check '{}'", transition.getSource().getStatus().getCode(), transition.getTarget().getStatus().getCode(), check.getId());

            try {
                checkBean.check(transition.getSource(), transition.getTarget());
            } catch (FailedCheckException e) {
                if (!check.getCanForce()) rejectTransition(transition, e);
                if (force) {
                    warnTransitionForced(transition, e);
                    continue;
                }
                requireConfirmation(transition, e);
            }
        }
    }

}
