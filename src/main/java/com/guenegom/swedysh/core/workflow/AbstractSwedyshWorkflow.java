package com.guenegom.swedysh.core.workflow;

import com.guenegom.swedysh.core.api.Swedysh;
import com.guenegom.swedysh.core.api.SwedyshStatus;
import com.guenegom.swedysh.core.api.SwedyshStatusFactory;
import com.guenegom.swedysh.core.api.WorkflowApi;
import com.guenegom.swedysh.core.exceptions.WorkflowConfigurationException;
import com.guenegom.swedysh.core.model.SwedyshTransitionConfiguration;
import com.guenegom.swedysh.core.model.SwedyshWorkflow;
import com.guenegom.swedysh.core.utils.SwedyshUtils;
import com.guenegom.swedysh.core.workflow.beans.ActualTransition;
import com.guenegom.swedysh.core.workflow.factories.StatusFactoryResolver;
import com.guenegom.swedysh.core.workflow.triggers.SimpleActionsTrigger;
import com.guenegom.swedysh.core.workflow.validators.SimpleValidator;
import org.apache.commons.collections4.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;

import javax.annotation.PostConstruct;
import java.util.Collections;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

public abstract class AbstractSwedyshWorkflow<T extends Swedysh> implements WorkflowApi<T> {

    private final SwedyshWorkflow workflow;
    @Autowired
    private SimpleValidator validator;
    // FIXME: Should be  SimpleActionsTrigger<T> but AbstractSwedyshWorkflowTest is unable to mock it correctly. I must be missing something...
    @Autowired
    private SimpleActionsTrigger trigger;
    @Autowired
    private WorkflowPostConstructor postConstructor;
    @Autowired
    private StatusFactoryResolver statusFactoryResolver;

    protected AbstractSwedyshWorkflow(SwedyshWorkflow workflow) {
        this.workflow = workflow;
    }

    @PostConstruct
    void processPostConstruct() {
        postConstructor.process(workflow);
    }

    @Override
    public final T executeTransition(T current, T target, boolean force) {
        Objects.requireNonNull(current);
        Objects.requireNonNull(target);
        SwedyshTransitionConfiguration transition = findMatchingTransition(current.getStatus(), target.getStatus());
        ActualTransition<T> actualTransition = new ActualTransition<>(current, target, transition);
        validator.validate(actualTransition, force);
        target = (T) trigger.trigger(actualTransition);
        return target;
    }

    @Override
    public List<SwedyshStatus> getCandidates(T current) {
        List<SwedyshTransitionConfiguration> transitions = findTransitionsToDisplayFromSource(current.getStatus());
        if (CollectionUtils.isEmpty(transitions)) {
            return Collections.emptyList();
        }

        SwedyshStatusFactory factory = statusFactoryResolver.resolveFactory(workflow.getGlobals().getFactory(), current.getStatus().getClass());

        return transitions.stream().map(t -> factory.instantiate(t.getTarget())).collect(Collectors.toList());
    }

    SwedyshTransitionConfiguration findMatchingTransition(SwedyshStatus source, SwedyshStatus target) {
        List<SwedyshTransitionConfiguration> matches = SwedyshUtils.findMatchingTransitions(workflow.getTransitions(), source, target);
        if (CollectionUtils.isEmpty(matches)) {
            return null;
        }
        if (matches.size() > 1) {
            throw new WorkflowConfigurationException(String.format("Several transitions match the provided statuses. Actual: %s[=source] %s[=target] Found %d transitions", source.getCode(), target.getCode(), matches.size()));
        }
        return matches.get(0);
    }

    private List<SwedyshTransitionConfiguration> findTransitionsToDisplayFromSource(SwedyshStatus source) {
//        List<SwedyshTransition> candidates = SwedyshUtils.findMatchingTransitions(workflow.getTransitions(), source, null);
//        return candidates.stream().filter(SwedyshDefaultConfiguration::getDisplay).collect(Collectors.toList());
        // No need to browse twice the transitions. The predicate should handle both conditions: "source equality + display is true"
        Objects.requireNonNull(source);
        return workflow.getTransitions().stream().filter(t -> source.getCode().equalsIgnoreCase(t.getSource()) && t.getDisplay()).collect(Collectors.toList());

    }
}
